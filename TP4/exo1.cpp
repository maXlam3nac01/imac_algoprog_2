#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    return 2 * nodeIndex + 1;
}

int Heap::rightChild(int nodeIndex)
{
    return 2 * nodeIndex + 2;
}

void Heap::insertHeapNode(int heapSize, int value)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i = heapSize;
    (*this)[i] = value;
    while (i > 0 && (*this)[i] > (*this)[(i - 1) / 2])
    {
        this->swap(i, (i - 1) / 2);
        i = (i - 1) / 2;
    }
}

void Heap::heapify(int heapSize, int nodeIndex)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i_max = nodeIndex;
    int Child_left = this->leftChild(nodeIndex);
    int Child_right = this->rightChild(nodeIndex);
    if (Child_left < heapSize && (*this)[Child_left] > (*this)[i_max])
    {
        i_max = Child_left;
    }
    if (Child_right < heapSize && (*this)[Child_right] > (*this)[i_max])
    {
        i_max = Child_right;
    }
    if ( i_max != nodeIndex)
    {
        this->swap(i_max, nodeIndex);
        heapify(heapSize, i_max);
    }
}

void Heap::buildHeap(Array& numbers)
{
    for (uint i = 0; i < numbers.size(); i++)
    {
        int temp = this->get(0);
        this->get(0) = this->get(i);
        this->get(i) = temp;
        heapify(i, 0);
    }
}

void Heap::heapSort()
{
    for (uint i = 0; i < this->size() - 1; i++)
    {
        this->heapify(this->size(), i);
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
